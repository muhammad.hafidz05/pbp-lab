1. Apakah perbedaan antara JSON dan XML?

- JSON memiliki memori file yang lebih ringan daripada XML
- JSON men-support tipe array sedangkan XML tidak
- JSON hanya men-support tipe data primitif sedangkan XML men-support tipe data primitif dan non-primitif
- Struktur kode pada JSON lebih sederhana seperti dictionary daripada XML yang seperti HTML
- Kecepatan parsing pada JSON lebih cepat daripada XML

2. Apakah perbedaan antara HTML dan XML?

- Jenis tag pada HTML terbatas sedangkan pada XML dapat dikembangkan sesuai dengan keinginan
- HTML tidak case sensitive sedangkan XML case sensitive
- HTML berfokus untuk menyajikan tampilan data kepada user sedangkan XML berfokus pada struktur dan konteks data.
