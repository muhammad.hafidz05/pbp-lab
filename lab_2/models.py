from django.db import models

# Create your models here.


class Note(models.Model):
    message_to = models.CharField(max_length=30, blank=True, null=True)
    message_from = models.CharField(max_length=30, blank=True, null=True)
    message_title = models.CharField(max_length=50, blank=True, null=True)
    message = models.CharField(max_length=100)
