import 'package:flutter/material.dart';
import 'package:lab_7/screens/survey_create_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Mini Survey - Corum',
      theme: ThemeData(primaryColor: const Color(0xFFC1FFD7)),
      home: const SurveyCreateScreen(),
    );
  }
}
