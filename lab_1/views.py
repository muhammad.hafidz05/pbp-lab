from django.shortcuts import render
from datetime import datetime, date
from .models import Friend

mhs_name = "Muhammad Hafidz Sulistyanto"  # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
# TODO Implement this, format (Year, Month, Date)
birth_date = date(2003, 1, 24)
npm = 2006536965  # TODO Implement this


def index(request):
    response = {'name': mhs_name,
                'age': calculate_age(birth_date.year),
                'npm': npm}
    return render(request, 'index_lab1.html', response)


def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0


def friend_list(request):
    friends = Friend.objects.all()  # TODO Implement this
    response = {'friends': friends}
    return render(request, 'friend_list_lab1.html', response)
