from django.shortcuts import render
from lab_2.models import Note

# Create your views here.


def index(request):
    notes = Note.objects.all()
    return render(request, 'lab5_.index.html', {'notes': notes})
