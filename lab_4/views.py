from django.http import request
from django.shortcuts import redirect, render
from lab_2.models import Note
from .forms import NoteForm

# Create your views here.


def index(request):
    notes = Note.objects.all()
    return render(request, 'lab4_index.html', {'notes': notes})


def add_note(request):
    form = NoteForm()

    if request.method == 'POST':
        form = NoteForm(request.POST)

        if form.is_valid():
            form.save()
            return redirect('/lab-4')

    return render(request, 'lab4_form.html', {'form': form})


def note_list(request):
    notes = Note.objects.all()
    return render(request, 'lab4_note_list.html', {'notes': notes})
