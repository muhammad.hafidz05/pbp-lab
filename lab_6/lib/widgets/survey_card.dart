import 'package:flutter/material.dart';
import 'package:lab_6/models/survey.dart';

class SurveyCard extends StatelessWidget {
  final Survey survey;

  const SurveyCard(this.survey, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 4),
      child: Card(
        clipBehavior: Clip.antiAlias,
        child: Padding(
          padding: const EdgeInsets.all(4.0),
          child: ExpansionTile(
            backgroundColor: const Color.fromRGBO(101, 204, 184, .8),
            title: Text(
              survey.title,
              style: const TextStyle(color: Colors.black),
            ),
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 6.0),
                child: Row(
                  children: [
                    Flexible(
                      child: Text(
                        _showDate(survey.pubDate),
                        style: const TextStyle(fontWeight: FontWeight.w300),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 16.0, bottom: 16.0),
                child: Row(
                  children: [
                    Flexible(
                      child: Text(
                        survey.description,
                      ),
                    ),
                  ],
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  TextButton(
                    child: const Text('Vote'),
                    onPressed: () {},
                    style: TextButton.styleFrom(
                      backgroundColor: const Color(0xFF3B845E),
                      primary: Colors.white),
                  ),
                  const SizedBox(width: 3),
                  TextButton(
                    child: const Text('View Result'),
                    onPressed: () {},
                    style: TextButton.styleFrom(
                      backgroundColor: const Color(0xFF3B845E),
                      primary: Colors.white),
                  ),
                ]
              ),
            ],
          ),
        ),
      ),
    );
  }

  String _showDate(DateTime date) {
    List MONTH = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ];

    int day = date.day;
    int month = date.month;
    int year = date.year;
    String monthName = MONTH[month - 1];
    String dateResult = 'Created on $day $monthName $year';

    return dateResult;
  }
}