class Survey {
  final String title;
  final String description;
  final String creator;
  final DateTime pubDate;

  const Survey({
    required this.title,
    required this.description,
    required this.creator,
    required this.pubDate,
  });
}
