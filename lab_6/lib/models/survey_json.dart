import 'package:lab_6/models/survey.dart';

class SurveyJson {
  final String model;
  final List<Survey> fields;

  SurveyJson({
    required this.model,
    required this.fields,
  });
}