import 'package:flutter/material.dart';
import 'package:lab_6/screens/survey_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  static Map<int, Color> themeColor = {
    50: const Color.fromRGBO(193,255,215, .1),
    100: const Color.fromRGBO(193,255,215, .2),
    200: const Color.fromRGBO(193,255,215, .3),
    300: const Color.fromRGBO(193,255,215, .4),
    400: const Color.fromRGBO(193,255,215, .5),
    500: const Color.fromRGBO(193,255,215, .6),
    600: const Color.fromRGBO(193,255,215, .7),
    700: const Color.fromRGBO(193,255,215, .8),
    800: const Color.fromRGBO(193,255,215, .9),
    900: const Color.fromRGBO(193,255,215, 1),
  };

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Mini Survey - Corum',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run"
        primarySwatch: MaterialColor(0xFFC1FFD7, themeColor),
      ),
      home: const SurveyHomePage(),
    );
  }
}
