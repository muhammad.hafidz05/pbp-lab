import 'package:lab_6/models/survey.dart';
import 'package:lab_6/models/survey_json.dart';

SurveyJson DUMMY_SURVEY = SurveyJson(
  model: 'survey.survey',
  fields: [
    Survey(
      title: 'Judul Survey 1',
      description: 'Ini deskripsi survey 1',
      creator: 'Hafidz',
      pubDate: DateTime.parse('2021-11-17'),
    ),
    Survey(
      title: 'Judul Survey 2',
      description: 'Ini deskripsi survey 2',
      creator: 'Sulistyanto',
      pubDate: DateTime.parse('2021-11-18')
    )
  ],
);
