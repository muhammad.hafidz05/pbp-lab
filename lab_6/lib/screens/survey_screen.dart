import 'package:flutter/material.dart';
import 'package:lab_6/dummy_data.dart';
import 'package:lab_6/models/survey.dart';
import 'package:lab_6/widgets/survey_card.dart';

class SurveyHomePage extends StatefulWidget {
  const SurveyHomePage({Key? key}) : super(key: key);

  @override
  State<SurveyHomePage> createState() => _SurveyHomePageState();
}

class _SurveyHomePageState extends State<SurveyHomePage> {
  List<Survey> dummySurvey = DUMMY_SURVEY.fields;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Corum'),
      ),
      drawer: Drawer(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Container(
              height: 80,
              width: double.infinity,
              color: const Color(0xFFC1FFD7),
              child: const DrawerHeader(
                child: Text(
                  'Corum',
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 24,
                  ),
                ),
              ),
            ),
            ListTile(
              title: const Text('Home'),
              onTap: () {},
            ),
            ListTile(
              title: const Text('Events'),
              onTap: () {},
            ),
            ListTile(
              title: const Text('Forum'),
              onTap: () {},
            ),
            ListTile(
              title: const Text('Blog'),
              onTap: () {},
            ),
            ListTile(
              title: const Text('Survey'),
              onTap: () {},
            )
          ],
        )
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 20.0),
              child: ElevatedButton(
                child: const Text(
                  'Create New Survey',
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.normal,
                    fontSize: 20,
                  ),
                ),
                onPressed: () {},
                style: ElevatedButton.styleFrom(
                  primary: Colors.blue,
                ),
              ),
            ),
            ListView.builder(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemCount: dummySurvey.length,
              itemBuilder: (context, index) {
                return SurveyCard(dummySurvey[index]);
              }
            )
          ],
        ),
      ),
    );
  }
}